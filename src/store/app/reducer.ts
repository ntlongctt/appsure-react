import { TypedMap, MapInterface } from '../../models/TypedMap';
import { User } from '../../models/User';

export interface AppState {
  currentUser: User;
  token: string;
}

const initState: MapInterface<AppState> = TypedMap({
  currentUser: {
    name: 'Phuc Co',
    email: 'cogiaphuc97@gmail.com',
    phone: '232425267',
    avatar: ''
  },
  token: ''
});

const appReducer = (state: MapInterface<AppState> = initState, action: any): MapInterface<AppState> => {
  switch(action.type) {
    default:
      return state;
  }
};

export default appReducer;
