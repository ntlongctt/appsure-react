export const TOGGLE_FOLDED_NAVBAR = '[SETTING] TOGGLE_FOLDED_NAVBAR';

export interface ToggleFoldedNavbarAction {
  type: typeof TOGGLE_FOLDED_NAVBAR;
}

export const toggleFoldedNavbar = (): ToggleFoldedNavbarAction => ({
  type: TOGGLE_FOLDED_NAVBAR
});

export type SettingAction = ToggleFoldedNavbarAction;
