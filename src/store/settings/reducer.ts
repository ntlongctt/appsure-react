import { createMuiTheme, Theme } from '@material-ui/core';
import merge from 'lodash/merge';
import themeConfigs, { defaultThemeOptions, mustHaveThemeOptions, extendThemeWithMixins, mainThemeVariations } from '../../configs/themeConfigs';
import settingsConfig from '../../configs/settingConfigs';
import layoutConfigs from '../../configs/layoutConfigs';
import { MapInterface, TypedMap } from '../../models/TypedMap';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';
import {SettingAction, TOGGLE_FOLDED_NAVBAR} from "./actions";

interface InitialSetting {
  customScrollbars: boolean;
  theme: {
    [key: string]: string;
  };
  layout: {
    config: {
      [key: string]: string | {
        [key: string]: string | boolean;
      }
    }
  }
}

interface InitialTheme {
  [key: string]: Theme
}

export interface SettingState {
  [key: string]: InitialSetting | InitialTheme
}

const initialSettings = getInitialSettings();
const initialThemes = getInitialThemes();

const initState: MapInterface<SettingState> = TypedMap({
  initial : initialSettings,
  current : merge({}, initialSettings),
  themes  : initialThemes,
  ...getThemeOptions(initialThemes, initialSettings)
});

const settingReducer = (state: MapInterface<SettingState> = initState, action: SettingAction): MapInterface<SettingState> => {
  const path = ['current', 'layout', 'config', 'navbar', 'folded'];
  switch (action.type) {
    case TOGGLE_FOLDED_NAVBAR:
      return state.setIn(path, !state.getIn(path));
    default:
      return state;
  }
};

function getInitialSettings(): InitialSetting {
  const layout = {
    config: layoutConfigs.defaults
  };
  return merge({}, settingsConfig, { layout });
}

function getInitialThemes(): InitialTheme {
  const themesObj = themeConfigs;

  const themes = Object.assign({}, ...Object.entries(themesObj).map(([key, value]) => {
      const muiTheme: ThemeOptions = merge({}, defaultThemeOptions, value, mustHaveThemeOptions);
      return {
        [key]: createMuiTheme(merge({}, muiTheme, { mixins: extendThemeWithMixins(muiTheme) }))
      }
    }
  ));

  return {
    ...themes,
    ...mainThemeVariations(themesObj[initialSettings.theme.main])
  }
}

function updateMainThemeVariations(mainTheme: string): {[key: string]: Theme} {
  return mainThemeVariations(themeConfigs[mainTheme])
}

function getThemeOptions(themes: InitialTheme, settings: InitialSetting): {[key: string]: Theme} {
  return {
    mainTheme   : themes[settings.theme.main],
    navbarTheme : themes[settings.theme.navbar],
    toolbarTheme: themes[settings.theme.toolbar],
    footerTheme : themes[settings.theme.footer],
    ...updateMainThemeVariations(settings.theme.main)
  }
}

export default settingReducer;
