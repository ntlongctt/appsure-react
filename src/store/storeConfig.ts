import {applyMiddleware, compose, createStore, Middleware, Store} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { logger } from 'redux-logger';
import { rootReducer } from './rootReducer';
import rootSaga from './rootSaga';

const isDev = process.env.NODE_ENV === 'development';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(): Store<any, any> {
  const middleWares: Middleware[] = [sagaMiddleware];

  let composeEnhancers;
  if (isDev) {
    composeEnhancers = composeWithDevTools;
    middleWares.push(logger);
  } else {
    composeEnhancers = compose as any;
  }

  const middleWareEnhancer = applyMiddleware(...middleWares);

  const store = createStore(rootReducer, composeEnhancers(middleWareEnhancer));
  sagaMiddleware.run(rootSaga);
  return store;
}
