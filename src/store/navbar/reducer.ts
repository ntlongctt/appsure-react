import { TypedMap, MapInterface } from '../../models/TypedMap';
import {
  CLOSE_FOLDED_NAVBAR, CLOSE_MOBILE_NAVBAR,
  NavbarAction,
  OPEN_FOLDED_NAVBAR, OPEN_MOBILE_NAVBAR,
  TOGGLE_FOLDED_NAVBAR,
  TOGGLE_MOBILE_NAVBAR
} from './action';


export interface NavBarState {
  foldedOpen: boolean;
  mobileOpen: boolean;
}

const initState: MapInterface<NavBarState> = TypedMap({
  foldedOpen: false,
  mobileOpen: false
});

const navbarReducer = (state: MapInterface<NavBarState> = initState, action: NavbarAction): MapInterface<NavBarState> => {
  switch (action.type) {
    case TOGGLE_FOLDED_NAVBAR:
      return state.set('foldedOpen', !state.get('foldedOpen'));
    case OPEN_FOLDED_NAVBAR:
      return state.set('foldedOpen', true);
    case CLOSE_FOLDED_NAVBAR:
      return state.set('foldedOpen', false);
    case TOGGLE_MOBILE_NAVBAR:
      return state.set('mobileOpen', !state.get('mobileOpen'));
    case OPEN_MOBILE_NAVBAR:
      return state.set('mobileOpen', true);
    case CLOSE_MOBILE_NAVBAR:
      return state.set('mobileOpen', false);
    default:
      return state;
  }
};

export default navbarReducer;
