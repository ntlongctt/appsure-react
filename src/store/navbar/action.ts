export const OPEN_FOLDED_NAVBAR = '[NAVBAR] OPEN FOLDED';
export const CLOSE_FOLDED_NAVBAR = '[NAVBAR] CLOSE FOLDED';
export const TOGGLE_FOLDED_NAVBAR = '[NAVBAR] TOGGLE FOLDED';
export const TOGGLE_MOBILE_NAVBAR = '[NAVBAR] TOGGLE MOBILE';
export const OPEN_MOBILE_NAVBAR = '[NAVBAR] OPEN MOBILE';
export const CLOSE_MOBILE_NAVBAR = '[NAVBAR] CLOSE MOBILE';

export interface NavbarAction {
  type: string;
}

export function navbarToggleFolded(): NavbarAction {
  return {
    type: TOGGLE_FOLDED_NAVBAR
  }
}

export function navbarOpenFolded(): NavbarAction {
  return {
    type: OPEN_FOLDED_NAVBAR
  }
}

export function navbarCloseFolded(): NavbarAction {
  return {
    type: CLOSE_FOLDED_NAVBAR
  }
}

export function navbarToggleMobile(): NavbarAction {
  return {
    type: TOGGLE_MOBILE_NAVBAR
  }
}

export function navbarOpenMobile(): NavbarAction {
  return {
    type: OPEN_MOBILE_NAVBAR
  }
}

export function navbarCloseMobile(): NavbarAction {
  return {
    type: CLOSE_MOBILE_NAVBAR
  }
}

