import { all, fork } from 'redux-saga/effects';

import { saga as userSaga } from './user/saga';


export default function* rootSaga(): Generator {
  yield all([fork(userSaga)]);
}
