import { all, put, takeEvery, call } from 'redux-saga/effects';
import { getUsersApi, removeUserApi } from '../../api/User';
import {
  GetUsersAction,
  GET_USERS,
  getUsersSuccess,
  removeUserSuccess,
  getUsersFailed,
  removeUserFailed,
  RemoveUserAction,
  REMOVE_USER
} from "./actions";
import { User } from "../../models/User";

export function* getUsersSaga(): Generator {
  try {
    const data = yield call(getUsersApi);
    yield put(getUsersSuccess(data as User[]));
  } catch (err) {
    yield put(getUsersFailed(err));
  }
}

export function* removeUserSaga(action: RemoveUserAction): Generator {
  try {
    const data = yield call(removeUserApi, action.payload);
    yield put(removeUserSuccess(data as User));
  } catch (err) {
    yield put(removeUserFailed(err));
  }
}

export function* saga(): Generator {
  yield all([takeEvery<GetUsersAction>(GET_USERS, getUsersSaga)]);
  yield all([takeEvery<RemoveUserAction>(REMOVE_USER, removeUserSaga)]);
}
