import { User } from '../../models/User';

/**  GET_USERS * */
export const GET_USERS = '[USER] GET_USER';
export const GET_USERS_SUCCESS = '[USER] GET_USERS_SUCCESS';
export const GET_USERS_FAILED = '[USER] GET_USERS_FAILED';

export interface GetUsersAction {
  type: typeof GET_USERS;
}

export const getUsers = (): GetUsersAction => ({
  type: GET_USERS
});

export interface GetUsersActionSuccess {
  type: typeof GET_USERS_SUCCESS;
  payload: User[];
}

export const getUsersSuccess = (payload: User[]): GetUsersActionSuccess => ({
  type: GET_USERS_SUCCESS,
  payload
});

export interface GetUsersActionFailed {
  type: typeof GET_USERS_FAILED;
  payload: string;
}

export const getUsersFailed = (error: string): GetUsersActionFailed => ({
  type: GET_USERS_FAILED,
  payload: error
});

/**  Remove User * */
export const REMOVE_USER = '[USER] REMOVE_USER';
export const REMOVE_USER_SUCCESS = '[USER] REMOVE_USER_SUCCESS';
export const REMOVE_USER_FAILED = '[USER] REMOVE_USER_FAILED';

export interface RemoveUserAction {
  type: typeof REMOVE_USER;
  payload: string
}

export const removeUser = (userId: string): RemoveUserAction => ({
  type: REMOVE_USER, payload: userId
});

export interface RemoveUserActionSuccess {
  type: typeof REMOVE_USER_SUCCESS;
  payload: User;
}

export const removeUserSuccess = (payload: User): RemoveUserActionSuccess => ({
  type: REMOVE_USER_SUCCESS,
  payload
});

export interface RemoveUserActionFailed {
  type: typeof REMOVE_USER_FAILED;
  payload: string;
}

export const removeUserFailed = (error: string): RemoveUserActionFailed => ({
  type: REMOVE_USER_FAILED,
  payload: error
});

export type UserAction =
  | GetUsersAction
  | GetUsersActionSuccess
  | GetUsersActionFailed
  | RemoveUserAction
  | RemoveUserActionSuccess
  | RemoveUserActionFailed;
