import { User } from '../../models/User';
import { TypedMap, MapInterface } from '../../models/TypedMap';
import {
  UserAction,
  GET_USERS,
  GET_USERS_SUCCESS,
  GET_USERS_FAILED,
  REMOVE_USER,
  REMOVE_USER_FAILED,
  REMOVE_USER_SUCCESS,
} from './actions';

export interface UserState {
  users: User[];
  loading: boolean;
  selectedUserId: string;
  error: string;
}

const initState: MapInterface<UserState> = TypedMap({
  users: [],
  error: '',
  loading: false,
  selectedUserId: ''
});

const userReducer = (state: MapInterface<UserState> = initState, action: UserAction): MapInterface<UserState> => {
  switch (action.type) {
    case REMOVE_USER:
      return state.set('loading', true);
    case REMOVE_USER_FAILED:
      return state.set('loading', false).set('error', action.payload);
    case REMOVE_USER_SUCCESS:
      return state.set('loading', false).set('users', state.get('users').filter((i: User): boolean => i.id !== action.payload.id));
    case GET_USERS:
      return state.set('loading', true);
    case GET_USERS_SUCCESS:
      return state.set('loading', false).set('users', action.payload);
    case GET_USERS_FAILED:
      return state.set('loading', false).set('error', action.payload);
    default:
      return state;
  }
};

export default userReducer;
