import { combineReducers } from 'redux';

import userReducer from './user/reducer';
import settingReducer from './settings/reducer';
import appReducer from './app/reducer';
import navbarReducer from './navbar/reducer';

export const rootReducer = combineReducers({
  user: userReducer,
  setting: settingReducer,
  app: appReducer,
  navbar: navbarReducer
});
