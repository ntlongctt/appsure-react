import { useEffect, useRef } from 'react'

const useTimeout = (callback: Function, delay: number | false): void => {
  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    if (delay && callback && typeof callback === 'function') {
      const timer = setTimeout(callbackRef.current, delay || 0);
      return (): void => {
        if (timer) {
          clearTimeout(timer);
        }
      };
    }
  }, [callback, delay]);
};

export default useTimeout;
