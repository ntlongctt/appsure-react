export const isArray = (arr: any): boolean => Array.isArray(arr);

export const isArrayNotEmpty = (arr: any): boolean =>
  isArray(arr) && arr.length;

export const isHasValue = (value: any): boolean =>
  value !== null && value !== undefined;

export const isNotEmpty = (value: any): boolean =>
  isHasValue(value) && !!value.toString().trim().length;
export const isNumeric = (n: string): boolean =>
  !Number.isNaN(Number.parseFloat(n)) && Number.isFinite(Number.parseFloat(n));

export const isNotEmptyObject = (value: any): boolean =>
  isHasValue(value) &&
  Object.entries(value).length > 0 &&
  value.constructor === Object;
