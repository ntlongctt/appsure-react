const TokenKey = 'appsure_token';

export const setToken = (token: string) =>
  localStorage.setItem(TokenKey, token);

export const getToken = (): string => localStorage.getItem(TokenKey) || '';

export const removeToken = () => localStorage.removeItem(TokenKey);
