export type Form = {
  [key: string]: FormField;
} & {
  _valid: boolean;
};

export interface ValidatorItem {
  type: string;
  validate: (...args: any[]) => boolean;
  defaultMessage: string;
}

export interface Validator {
  [key: string]: (...args: any[]) => ValidatorItem;
}

export interface FormField {
  value: any;
  valid: boolean;
  invalidTypes: string[];
  messages: string[];
  validators: ValidatorItem[];
}
