import { get } from 'lodash';
import { isNotEmpty } from '../Utils';

export default {
  required: (options?: { message?: string }) => ({
    type: 'required',
    validate: (value: any): boolean => isNotEmpty(value),
    message: get(options, 'message')
  })
};
