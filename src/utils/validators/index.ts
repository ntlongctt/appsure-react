import { get, merge, omit } from 'lodash';
import { Form, ValidatorItem, FormField } from './types';
import appValidators from './validators';
import { isArray } from '../Utils';

/**
 *
 * @param fields object of {fieldName: array with first item is initial value and second is array of validators}
 */
export const buildForm = (fields: { [key: string]: any }): Form => {
  const form = { _valid: true };

  Object.keys(fields).forEach((fieldName: string) => {
    const fieldData = fields[fieldName];

    const [value, validators = []] = isArray(fieldData)
      ? fieldData
      : [fieldData, []];

    const formField = {
      [fieldName]: initField(value, validators)
    };
    merge(form, formField);
  });
  return form as Form;
};

export const initField = (
  value: any,
  validators: ValidatorItem[]
): FormField => ({
  value,
  valid: true,
  invalidTypes: [],
  messages: [],
  validators
});

export const checkForm = (form: Form): Form => {
  let valid = true;
  const fields = omit(form, ['_valid']);

  Object.keys(fields).forEach((key: string) => {
    fields[key] = checkField(get(fields, key));
    if (!fields[key].valid) {
      valid = false;
    }
  });

  return {
    _valid: valid,
    ...fields
  } as Form;
};

export const checkField = (field: FormField) => {
  const messages: any[] = [];
  const invalidTypes: any[] = [];
  let valid = true;

  const { value } = field;

  field.validators.forEach((type: any) => {
    const typeValid = type.validate(value);

    if (!typeValid) {
      valid = false;
      invalidTypes.push(type.type);
      messages.push(type.message);
    }
  });

  return {
    ...field,
    messages,
    valid,
    invalidTypes
  };
};

export const changeFieldValue = (form: Form, key: string, value: any) => {
  const field = { ...form[key] as FormField, value };
  return {
    ...form,
    [key]: checkField(field)
  };
};

/**
 * fields is array of field change, with first item is field name, second item is valu
 */
export const changeArrayFieldValue = (form: Form, ...fields: any[]) => {
  const fieldsUpdated = {};
  fields.forEach(([key, value]) => {
    const fieldUpdated = { ...form[key] as FormField, value };
    merge(fieldsUpdated, fieldUpdated);
  });

  return {
    ...form,
    fieldsUpdated
  };
};

export const getFormValue = (form: Form): { [key: string]: any } => {
  const fields = omit(form, ['_valid']);

  return Object.keys(fields).reduce((value: object, key: string) => {
    merge(value, {
      [key]: get(fields, `${key}.value`)
    });

    return value;
  }, {});
};

export const Validators = appValidators;
