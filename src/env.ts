import { cleanEnv, str } from 'envalid';

export default cleanEnv(process.env, {
  REACT_APP_API_URL: str()
});
