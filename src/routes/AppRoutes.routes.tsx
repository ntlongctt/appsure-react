import React from 'react';
import { Router, Route } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/styles';
import { useSelector } from 'react-redux';
import { createBrowserHistory } from 'history';
import Suspense from '../components/Suspense';
import { ConnectState } from '../models/ConnectState';
import MainLayout from '../layout/MainLayout';

const history = createBrowserHistory();

const routes = [
  {
    path: '/',
    component: React.lazy(() => import('../pages/Home'))
  },
  {
    path: '/users',
    component: React.lazy(() => import('../pages/User'))
  }
];

const Approutes: React.FC<{}> = () => {
  const mainTheme = useSelector((state: ConnectState) => state.setting.get('mainTheme'));
  return (
    <Router history={history}>
      <Suspense>
        <ThemeProvider theme={mainTheme}>
          <MainLayout>
            <>
              {routes.map(route =>
                <Route
                  key={`route-to-${route.path.split('/').join('-')}`}
                  exact={true}
                  path={route.path}
                  component={route.component}
                />
              )}
            </>
          </MainLayout>
        </ThemeProvider>
      </Suspense>
    </Router>
  );
};

export default Approutes;
