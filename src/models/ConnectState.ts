import { MapInterface } from './TypedMap';
import { UserState } from '../store/user/reducer';
import { SettingState } from '../store/settings/reducer';
import { AppState } from '../store/app/reducer';
import { NavBarState } from '../store/navbar/reducer';

export interface ConnectState {
  user: MapInterface<UserState>;
  setting: MapInterface<SettingState>;
  app: MapInterface<AppState>;
  navbar: MapInterface<NavBarState>;
}
