import { Map } from 'immutable';

export interface MapInterface<T> extends Map<string, any> {
  get<K extends keyof T>(name: K): T[K];
}

export function TypedMap<T>(value: T): MapInterface<T> {
  return Map(value) as MapInterface<T>;
}
