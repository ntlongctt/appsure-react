export interface BaseModel {
  createdAt: string;
  id: string;
}

export interface User extends Partial<BaseModel> {
  email: string;
  name: string;
  phone: string;
  avatar?: string;
}
