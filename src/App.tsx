import React from 'react';
import { Provider } from 'react-redux';
import { StylesProvider, jssPreset, createGenerateClassName } from '@material-ui/styles';
import { create } from 'jss';
import jssExtend from 'jss-extend';
import './App.css';
import Approutes from './routes/AppRoutes.routes';
import configureStore from './store/storeConfig'

const jss = create({
  ...jssPreset(),
  plugins       : [...jssPreset().plugins, jssExtend()],
  insertionPoint: document.getElementById('jss-insertion-point') || '',
});

const generateClassName = createGenerateClassName();

const store = configureStore();

const App: React.FC = () =>
  <StylesProvider jss={jss} generateClassName={generateClassName}>
    <Provider store={store}>
      <Approutes />
    </Provider>
  </StylesProvider>;

export default App;
