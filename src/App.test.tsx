import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from './tests/enzymer';
import App from './App';

it('renders without crashing', () => {
  // const wrapper = shallow(<App />);
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
