export interface Navigation {
  id: string;
  children?: Navigation[];
  title: string;
  type: string;
  icon: string;
  url?: string;
  badge?: NavigationBadge;
  exact?: boolean;
  target?: string;
}

export interface NavigationBadge {
  title: JSX.Element;
  bg: string;
  fg: string;
}

const navigationConfigs: Navigation[] = [
  {
    'id'      : 'applications',
    'title'   : 'Applications',
    'type'    : 'group',
    'icon'    : 'apps',
    'children': [
      {
        'id'   : 'dashboard-component',
        'title': 'Dashboard',
        'type' : 'item',
        'icon' : 'whatshot',
        'url'  : '/dashboard'
      }
    ]
  }
];

export default navigationConfigs;
