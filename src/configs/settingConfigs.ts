const settingsConfig = {
  customScrollbars: true,
  theme           : {
    main   : 'default',
    navbar : 'mainThemeDark',
    toolbar: 'mainThemeLight',
    footer : 'mainThemeDark'
  }
};

export default settingsConfig;
