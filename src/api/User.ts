import { apiGet, apiDelete } from './BaseAPI';
import { User } from '../models/User';

export const getUsersApi = async (): Promise<User[] |  null> => apiGet('User')

export const removeUserApi = async (userId: string): Promise<User> => apiDelete(`User/${userId}`)
