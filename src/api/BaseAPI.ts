import axios, { AxiosRequestConfig } from 'axios';
import { getToken, removeToken } from '../utils/Storage';
import env from '../env';
import { get } from 'lodash';
import { stringify } from 'querystring';

enum HttpMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE'
}

interface RequestConfigs {
  method: HttpMethod;
  url: string;
  params?: any;
  hasToken?: boolean;
  data?: any;
  options?: AxiosRequestConfig;
}

const requestHandler = (request: any) => {
  // make somethings with request
  console.log('%c Send request', 'color: orange');
  return request;
};

const successHandler = (respone: any) => {
  console.log('%c Request success', 'color: green');
  return respone.data || respone;
};

const errorHandler = (error: any): void => {
  const errData = get(error, 'response.data');
  const status = get(error, 'response.status');
  console.log('%c Request error', 'color: red');
  if (status === 401) {
    removeToken();
  }

  if (errData) {
    throw errData;
  }
  throw error;
};

const getHeaderToken = (): string => {
  const token = getToken();
  if (!token) {
    return '';
  }
  return `Token ${token}`;
};

const apiRequest = (configs: RequestConfigs): Promise<any> => {
  const config: AxiosRequestConfig = {
    method: configs.method,
    baseURL: env.REACT_APP_API_URL,
    url: configs.url,
    params: configs.params,
    data: configs.data,
    headers: {
      // Authorization: configs.hasToken ? getHeaderToken() : '',
    },
    timeout: 10 * 1000,
    maxRedirects: 5,
    ...configs.options
  };

  requestHandler(config);

  return axios
    .request(config)
    .then(rs => successHandler(rs))
    .catch(err => errorHandler(err));
};

export const apiGet = (
  url: string,
  params: any = null,
  hasToken?: boolean,
  options?: AxiosRequestConfig
): Promise<any> => {
  const configs: RequestConfigs = {
    method: HttpMethod.GET,
    url,
    params,
    hasToken,
    options: {
      paramsSerializer: (par: any): string => {
        const stringified = stringify(par) || '';
        return stringified.replace(/[^=&]+=(&|$)/g, '').replace(/&$/, '');
      },
      ...options
    }
  };
  return apiRequest(configs);
};

export const apiPost = (
  url: string,
  data?: any,
  hasToken?: boolean,
  options?: AxiosRequestConfig
): Promise<any> => {
  const configs: RequestConfigs = {
    method: HttpMethod.POST,
    url,
    data,
    hasToken,
    options
  };
  return apiRequest(configs);
};

export const apiPut = (
  url: string,
  data?: any,
  hasToken?: boolean,
  options?: AxiosRequestConfig
): Promise<any> => {
  const configs: RequestConfigs = {
    method: HttpMethod.PUT,
    url,
    data,
    hasToken,
    options
  };
  return apiRequest(configs);
};

export const apiPatch = (
  url: string,
  data?: any,
  hasToken?: boolean,
  options?: AxiosRequestConfig
): Promise<any> => {
  const configs: RequestConfigs = {
    method: HttpMethod.PATCH,
    url,
    data,
    hasToken,
    options
  };
  return apiRequest(configs);
};

export const apiDelete = (
  url: string,
  hasToken?: boolean,
  options?: AxiosRequestConfig
): Promise<any> => {
  const configs: RequestConfigs = {
    method: HttpMethod.DELETE,
    url,
    hasToken,
    options
  };
  return apiRequest(configs);
};

export const apiPostForm = (url: string, data: any): Promise<any> => fetch(url, {
    body: data,
    method: 'post',
    headers: {
      Authorization: getHeaderToken()
    }
  });
