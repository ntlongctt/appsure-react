import React from 'react';
import { shallow } from '../../tests/enzymer';
import { } from 'history';
import Home from './Home';

describe('Home Page render correct', () => {
  const wrapper = shallow(<Home />);

  it('should have a text', () => {
    const textEl = wrapper.find('h1');
    // should have only one text
    expect(textEl).toHaveLength(1);

    // text should be 'Home Page'
    expect(textEl.text()).toEqual('Home Page');
  });

  it('should have a button', () => {
    const gotoUserBtn = wrapper.find('#gotoUser');
    const btnEls = wrapper.find('button');

    // should have only one button
    expect(btnEls).toHaveLength(1);

    // button label should be 'Go to Users'
    expect(gotoUserBtn.text()).toEqual('Go to Users');

    // button should not disable
    expect(gotoUserBtn.prop('disabled')).not.toBe(true);
  });
});
