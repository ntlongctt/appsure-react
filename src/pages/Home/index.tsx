import React from 'react';
import { History } from 'history';
import { makeStyles, StyleRules, Theme } from '@material-ui/core';

interface Props {
  history: History;
}

const useStyles = makeStyles((theme: Theme): StyleRules => ({
  root: {
    display: 'block'
  },
  h1: {
    color: theme.palette.secondary.dark
  }
}));

const Home: React.FC<Props> = (props: Props) => {
  const gotoUsers = (): void => props.history.push('users');
  const classes = useStyles(props);
  return (
    <div className={classes.root}>
      <h1 className={classes.h1}>Home Page</h1>
      <button onClick={gotoUsers}>Go to Users</button>
    </div>
  );
};

export default Home;
