import React from 'react';
import { History } from 'history';
import AppSureInput from '../../components/Input';
import { Form } from '../../utils/validators/types';
import { buildForm, Validators, changeFieldValue, checkForm } from '../../utils/validators';

interface Props {
  history?: History;
  gotoUsers?: () => void;
  changeField?: () => void;
}

interface State {
  form: Form
}

class Home extends React.Component<Props, State> {

  state: State = {
    form: buildForm({
      email: ['', [Validators.required({ message: 'This filed is required' }), Validators.required({ message: 'wew dw4dd' })]]
    }),
  }

  gotoUsers: () => void = () => {
    // this.props.history && this.props.history.push('users');
    const testForm = checkForm(this.state.form);
  }

  onChangeField: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void = event => {
    const formVal = changeFieldValue(this.state.form, event.currentTarget.name, event.currentTarget.value);
    this.setState({ form: formVal });
  }

  render(): JSX.Element {
    return (
      <>
        <h1>Home Page</h1>
        <button id="gotoUser" onClick={this.gotoUsers}>Go to Users</button>
        <AppSureInput
          field={this.state.form.email}
          label="My Textfield"
          name="email"
          onChange={this.onChangeField}
        />
      </>
    );
  }
}

export default Home;
