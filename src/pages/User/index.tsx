import React from 'react';
import { connect } from 'react-redux';
import {
  getUsers,
  GetUsersAction,
  removeUser,
  RemoveUserAction
} from '../../store/user/actions';
import { ConnectState } from '../../models/ConnectState';
import { User } from '../../models/User';

interface DispatchProps {
  getUsers: () => GetUsersAction;
  removeUser: (id: string) => RemoveUserAction;
}

interface StateProps {
  users: User[];
}

type Props = DispatchProps & StateProps;

const Index: React.FC<Props> = (props: Props) => {
  const onDeleteUser = (userId: string | undefined) => (): void => {
    userId && props.removeUser(userId);
  };

  return (
    <>
      <div>User</div>
      <button onClick={props.getUsers}>Get Users</button>
      <table>
        <tbody>
          {props.users &&
            props.users.map(user =>
              <tr key={user.id}>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.phone}</td>
                <td>
                  <button onClick={onDeleteUser(user.id)}>Delete</button>
                </td>
              </tr>
            )}
        </tbody>
      </table>
    </>
  );
};

const mapStateToProps = (state: ConnectState): StateProps => ({
  users: state.user.get('users')
});

const mapDispatchToProps: DispatchProps = {
  getUsers,
  removeUser
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
