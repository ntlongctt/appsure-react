import React from 'react';
import TextField, { StandardTextFieldProps } from '@material-ui/core/TextField';

interface Props extends StandardTextFieldProps {
  field: any;
}

const AppSureInput: React.FC<Props> = ({ field, ...props }) =>
  <div>
    <TextField
      error={!field.valid}
      helperText={field.messages.map((i: string) => <span key={i}>{i} <br /></span>)}
      {...props}
      />
  </div>

export default AppSureInput;
