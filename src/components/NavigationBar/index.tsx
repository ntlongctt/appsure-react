import React from 'react';
import { Drawer, Hidden, makeStyles, StyleRules, Theme, ThemeProvider } from '@material-ui/core';
import { ConnectState } from '../../models/ConnectState';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import NavigationBarContent from '../NavigationBarContent';
import { NavbarAction, navbarCloseFolded, navbarCloseMobile, navbarOpenFolded } from '../../store/navbar/action';
import { SettingAction, toggleFoldedNavbar } from '../../store/settings/actions';

const NAVBAR_WIDTH = 280;

const useStyles = makeStyles((theme: Theme): StyleRules => ({
  wrapper        : {
    display                     : 'flex',
    flexDirection               : 'column',
    zIndex                      : 4,
    [theme.breakpoints.up('lg')]: {
      width   : NAVBAR_WIDTH,
      minWidth: NAVBAR_WIDTH
    }
  },
  wrapperFolded  : {
    [theme.breakpoints.up('lg')]: {
      width   : 64,
      minWidth: 64
    }
  },
  navbar         : {
    display      : 'flex',
    overflow     : 'hidden',
    flexDirection: 'column',
    flex         : '1 1 auto',
    width        : NAVBAR_WIDTH,
    minWidth     : NAVBAR_WIDTH,
    height       : '100%',
    zIndex       : 4,
    transition   : theme.transitions.create(['width', 'min-width'], {
      easing  : theme.transitions.easing.sharp,
      duration: theme.transitions.duration.shorter
    }),
    boxShadow    : theme.shadows[3],
  },
  left           : {
    left: 0
  },
  right          : {
    right: 0
  },
  folded         : {
    position: 'absolute',
    width   : 64,
    minWidth: 64,
    top     : 0,
    bottom  : 0
  },
  opened: {
    width   : NAVBAR_WIDTH,
    minWidth: NAVBAR_WIDTH
  },
  navbarContent  : {
    flex: '1 1 auto',
  },
  closed: {
    '& $navbarContent': {
      '& .logo-icon'                                   : {
        width : 32,
        height: 32
      },
      '& .logo-text'                                   : {
        opacity: 0
      },
      '& .react-badge'                                 : {
        opacity: 0
      },
      '& .list-item-text, & .arrow-icon, & .item-badge': {
        opacity: 0
      },
      '& .list-subheader .list-subheader-text'         : {
        opacity: 0
      },
      '& .list-subheader:before'                       : {
        content  : '""',
        display  : 'block',
        position : 'absolute',
        minWidth : 16,
        borderTop: '2px solid',
        opacity  : .2
      },
      '& .collapse-children'                           : {
        display: 'none'
      },
      '& .user'                                        : {
        '& .username, & .email': {
          opacity: 0
        },
        '& .avatar'            : {
          width  : 40,
          height : 40,
          top    : 32,
          padding: 0
        }
      },
      '& .list-item.active'                            : {
        marginLeft  : 12,
        width       : 40,
        padding     : 12,
        borderRadius: 20,
        '&.square'  : {
          borderRadius: 0,
          marginLeft  : 0,
          paddingLeft : 24,
          width       : '100%'
        }
      }
    }
  }
}));

const NavigationBar: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const navbarTheme: any = useSelector((state: ConnectState) => state.setting.get('navbarTheme'));
  const folded: boolean = useSelector((state: ConnectState) => state.setting.getIn(['current', 'layout', 'config', 'navbar', 'folded']));
  const navbar: any = useSelector((state: ConnectState) => state.navbar);
  const classes = useStyles();

  const foldedAndClosed = folded && !navbar.get('foldedOpen');
  const foldedAndOpened = folded && navbar.get('foldedOpen');

  const onMouseEnter = (): NavbarAction | boolean => foldedAndClosed && dispatch(navbarOpenFolded());

  const onMouseLeave = (): NavbarAction | boolean => foldedAndOpened && dispatch(navbarCloseFolded());

  const onCloseMobileNavbar = (): NavbarAction => dispatch(navbarCloseMobile());

  const onToggleFolded = (): SettingAction => dispatch(toggleFoldedNavbar());

  return (
    <ThemeProvider theme={navbarTheme}>
      <div className={clsx(classes.wrapper,
        folded && classes.wrapperFolded)}>
        <Hidden mdDown>
          <div
            className={clsx(
              classes.navbar,
              classes.left,
              folded && classes.folded,
              foldedAndOpened && classes.opened,
              foldedAndClosed && classes.closed
            )}
            style={{ backgroundColor: navbarTheme.palette.background.default }}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}>
            <NavigationBarContent className={classes.navbarContent} onClick={onToggleFolded} />
          </div>
        </Hidden>
        <Hidden lgUp>
          <Drawer
            anchor="left"
            variant="temporary"
            open={navbar.get('mobileOpen')}
            classes={{
              paper: classes.navbar
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}>
            <NavigationBarContent className={classes.navbarContent} onClick={onCloseMobileNavbar} />
          </Drawer>
        </Hidden>
      </div>
    </ThemeProvider>
  )
};

export default NavigationBar;
