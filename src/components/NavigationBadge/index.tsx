import React from 'react';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import { NavigationBadge as NavigationBadgeType } from '../../configs/navigationConfigs';
import { StyleRules, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme): StyleRules => ({
  root: {
    padding        : '0 7px',
    fontSize       : 11,
    fontWeight     : 600,
    height         : 20,
    minWidth       : 20,
    borderRadius   : 20,
    display        : 'flex',
    alignItems     : 'center',
    backgroundColor: theme.palette.secondary.main,
    color          : theme.palette.secondary.contrastText
  }
}));

interface Props {
  badge: NavigationBadgeType;
  className?: string;
}

const NavigationBadge: React.FC<Props> = (props: Props) => {
  const classes = useStyles(props);
  const { className, badge } = props;

  return (
    <div
      className={clsx(classes.root, className, 'item-badge')}
      style={{
        backgroundColor: badge.bg,
        color          : badge.fg
      }}>
      {badge.title}
    </div>
  )
};

export default React.memo(NavigationBadge);
