import React, { PropsWithChildren } from 'react';
import { Icon, IconButton } from '@material-ui/core';

interface Props {
  className: string;
  onClick: () => void;
}

const NavbarMobileToggleButton = (props: PropsWithChildren<Props>): JSX.Element =>
    <IconButton
      className={props.className}
      onClick={props.onClick}
      color="inherit"
      disableRipple>
      {props.children}
    </IconButton>;


NavbarMobileToggleButton.defaultProps = {
  children: <Icon>menu</Icon>
};

export default NavbarMobileToggleButton;
