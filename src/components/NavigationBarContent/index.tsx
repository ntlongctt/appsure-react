import React from 'react';
import { AppBar, Hidden, Icon } from '@material-ui/core';
import clsx from 'clsx';
import NavbarMobileToggleButton from '../NavbarMobileToggleButton';
import { makeStyles } from '@material-ui/styles';
import Navigation from '../Navigation';
import ScrollBar from '../ScrollBar';
import NavbarFoldedToggleButton from '../NavbarFoldedToggleButton';

const useStyles = makeStyles({
  content: {
    overflowX                   : 'hidden',
    overflowY                   : 'auto',
    '-webkit-overflow-scrolling': 'touch',
    background                  : 'linear-gradient(rgba(0, 0, 0, 0) 30%, rgba(0, 0, 0, 0) 30%), linear-gradient(rgba(0, 0, 0, 0.25) 0, rgba(0, 0, 0, 0) 40%)',
    backgroundRepeat            : 'no-repeat',
    backgroundSize              : '100% 40px, 100% 10px',
    backgroundAttachment        : 'local, scroll',
  }
});

interface Props {
  className: string;
  onClick?: () => void;
}

const NavigationBarContent: React.FC<Props> = (props: Props) => {
  const classes = useStyles();
  const { className, onClick } = props;

  return (
    <div className={clsx('flex flex-col overflow-hidden h-full', className)}>
      <AppBar
        color="primary"
        position="static"
        elevation={0}
        className="flex flex-row justify-end items-center flex-shrink h-64 min-h-64 pl-20 pr-12">
        {onClick && <Hidden mdDown>
          <NavbarFoldedToggleButton className="w-40 h-40 p-0" onClick={onClick} />
        </Hidden>}
        {onClick && <Hidden lgUp>
          <NavbarMobileToggleButton className="w-40 h-40 p-0" onClick={onClick}>
            <Icon>arrow_back</Icon>
          </NavbarMobileToggleButton>
        </Hidden>}
      </AppBar>
      <ScrollBar enable={false} className={classes.content}>
        <Navigation />
      </ScrollBar>
    </div>
  );
};

export default NavigationBarContent;
