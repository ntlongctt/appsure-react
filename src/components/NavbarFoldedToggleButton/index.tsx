import React, { PropsWithChildren } from 'react';
import { Icon, IconButton } from '@material-ui/core';

interface Props {
  className?: string;
  onClick?: () => void;
}

const NavbarFoldedToggleButton = (props: PropsWithChildren<Props>): JSX.Element =>
    <IconButton
      className={props.className}
      color="inherit"
      onClick={props.onClick}>
      {props.children}
    </IconButton>;


NavbarFoldedToggleButton.defaultProps = {
  children: <Icon>menu</Icon>
};

export default NavbarFoldedToggleButton;
