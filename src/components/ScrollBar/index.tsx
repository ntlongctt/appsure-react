import React, { createRef, useEffect, useRef, useCallback } from 'react';
import PerfectScrollbar from 'perfect-scrollbar';
import 'perfect-scrollbar/css/perfect-scrollbar.css';
import MobileDetect from 'mobile-detect';

const md = new MobileDetect(window.navigator.userAgent);
const isMobile = md.mobile();

type Event =
  'onScrollY' |
  'onScrollX' |
  'onScrollUp' |
  'onScrollDown' |
  'onScrollLeft' |
  'onScrollRight' |
  'onYReachStart' |
  'onYReachEnd' |
  'onXReachStart' |
  'onXReachEnd';

const handlerNameByEvent: {[key: string]: Event} = {
  'ps-scroll-y': 'onScrollY',
  'ps-scroll-x': 'onScrollX',
  'ps-scroll-up': 'onScrollUp',
  'ps-scroll-down': 'onScrollDown',
  'ps-scroll-left': 'onScrollLeft',
  'ps-scroll-right': 'onScrollRight',
  'ps-y-reach-start': 'onYReachStart',
  'ps-y-reach-end': 'onYReachEnd',
  'ps-x-reach-start': 'onXReachStart',
  'ps-x-reach-end': 'onXReachEnd'
};
Object.freeze(handlerNameByEvent);

interface Props {
  className?: string;
  enable?: boolean;
  scrollToTopOnChildChange?: boolean;
  option?: {
    wheelPropagation: boolean;
  },
  onScrollY?: Function,
  onScrollX?: Function,
  onScrollUp?: Function,
  onScrollDown?: Function,
  onScrollLeft?: Function,
  onScrollRight?: Function,
  onYReachStart?: Function,
  onYReachEnd?: Function,
  onXReachStart?: Function,
  onXReachEnd?: Function
}

const ScrollBar = React.forwardRef((props: any, ref: any) => {
  // TODO: REMOVE ALL any TYPE
  ref = ref || createRef();
  const ps: any = useRef(null);
  const handlerByEvent = useRef(new Map());

  const hookUpEvents = useCallback(() => {
    Object.keys(handlerNameByEvent).forEach(key => {
      const callback = props[handlerNameByEvent[key]];
      if (callback) {
        const handler = (): void => callback(ref.current);
        handlerByEvent.current.set(key, handler);
        ref.current.addEventListener(key, handler, false);
      }
    });
  }, [ref, props]);

  const unHookUpEvents = useCallback(() => {
    Object.keys(handlerByEvent.current).forEach((value, key) => {
      if (ref.current) {
        ref.current.removeEventListener(key, value, false);
      }
    });
    handlerByEvent.current.clear();
  }, [ref]);

  const destroyPs = useCallback(() => {
    unHookUpEvents();

    if (!ps.current) {
      return;
    }
    ps.current.destroy();
    ps.current = null;
  }, [unHookUpEvents]);

  const createPs = useCallback(() => {
    if (isMobile || !ref || ps.current) {
      return;
    }

    ps.current = new PerfectScrollbar(ref.current, props.option);

    hookUpEvents();
  }, [hookUpEvents, props.option, ref]);

  useEffect(() => {
    function updatePs(): void {
      if (!ps.current) {
        return;
      }
      ps.current.update();
    }

    updatePs();
  });

  useEffect(() => {
    createPs();
  }, [createPs, destroyPs]);

  useEffect(() => {
    function scrollToTop(): void {
      ref.current.scrollTop = 0;
    }

    if (props.scrollToTopOnChildChange) {
      scrollToTop();
    }
  }, [props.children, props.scrollToTopOnChildChange, ref]);

  useEffect(() => (): void => {
    destroyPs();
  }, [destroyPs]);

  return (
    <div
      className={props.className}
      style={
        props.enable && !isMobile ?
          {
            position: 'relative',
            overflow: 'hidden'
          } : {}
      }
      ref={ref}>
      {props.children}
    </div>
  );
});

ScrollBar.defaultProps = {
  className: '',
  enable: true,
  scrollToTopOnChildChange: false,
  option: {
    wheelPropagation: true
  },
  ref: undefined,
  onScrollY: undefined,
  onScrollX: undefined,
  onScrollUp: undefined,
  onScrollDown: undefined,
  onScrollLeft: undefined,
  onScrollRight: undefined,
  onYReachStart: undefined,
  onYReachEnd: undefined,
  onXReachStart: undefined,
  onXReachEnd: undefined
};

export default React.memo(ScrollBar);
