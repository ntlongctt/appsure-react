import React from 'react';
import { Avatar } from '@material-ui/core';

interface Props {
  src?: string,
  userName: string
}

const UserAvatar: React.FC<Props> = (props: Props) => {
  if (!props.src) {
    const initials = props.userName.split(' ').map(n => n[0]).join('');
    return <Avatar>{initials}</Avatar>
  }
  return (
    <Avatar src={props.src} alt="User avatar" />
  )
};

export default UserAvatar;
