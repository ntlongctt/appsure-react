import React from 'react';
import Loading from '../Loading';

interface Props {
  delay?: Partial<number | false>;
  children: JSX.Element | null;
}

const Suspense: React.FC<Props> = (props: Props) =>
    <React.Suspense fallback={<Loading delay={0} />}>
      {props.children}
    </React.Suspense>
  ;

export default Suspense;
