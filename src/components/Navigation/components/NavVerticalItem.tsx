import React from 'react';
import { Icon, ListItem, ListItemText, StyleRules, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import NavLinkAdapter from './NavLinkAdapter';
import { withRouter } from 'react-router-dom';
import clsx from 'clsx';
import NavigationBadge from '../../NavigationBadge';
import { Navigation } from '../../../configs/navigationConfigs';
import { RouteConfigComponentProps } from 'react-router-config';

const useStyles = makeStyles((theme: Theme): StyleRules => ({
  item: {
    height                     : 40,
    width                      : 'calc(100% - 16px)',
    borderRadius               : '0 20px 20px 0',
    paddingRight               : 12,
    '&.active'                 : {
      backgroundColor            : theme.palette.secondary.main,
      color                      : theme.palette.secondary.contrastText + '!important',
      pointerEvents              : 'none',
      transition                 : 'border-radius .15s cubic-bezier(0.4,0.0,0.2,1)',
      '& .list-item-text-primary': {
        color: 'inherit'
      },
      '& .list-item-icon'        : {
        color: 'inherit'
      }
    },
    '&.square, &.active.square': {
      width       : '100%',
      borderRadius: '0'
    },
    '& .list-item-icon'        : {},
    '& .list-item-text'        : {},
    color                      : theme.palette.text.primary,
    cursor                     : 'pointer',
    textDecoration             : 'none!important'
  }
}));

interface Props extends RouteConfigComponentProps {
  item: Navigation
}

const _NavVerticalItem: React.FC<Props> = (props: Props) => {
  // const dispatch = useDispatch();
  const classes = useStyles();
  const { item } = props;

  return (
    <ListItem
      button
      component={NavLinkAdapter}
      to={item.url}
      activeClassName="active"
      className={clsx(classes.item, 'pl-24 list-item')}
      // onClick={ev => dispatch(Actions.navbarCloseMobile())}
      exact={item.exact}>
      {item.icon &&
        <Icon className="list-item-icon text-16 flex-shrink-0 mr-16" color="action">{item.icon}</Icon>
      }
      <ListItemText className="list-item-text" primary={item.title} classes={{ primary: 'text-14 list-item-text-primary' }} />
      {item.badge &&
        <NavigationBadge badge={item.badge} />
      }
    </ListItem>
  );
};

const NavVerticalItem = withRouter(React.memo(_NavVerticalItem));

export default NavVerticalItem;
