import React, { useEffect, useState } from 'react';
import { Collapse, Icon, IconButton, ListItem, ListItemText, StyleRules, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import clsx from 'clsx';
import { Navigation } from '../../../configs/navigationConfigs';
import NavigationBadge from '../../NavigationBadge';
import NavVerticalGroup from './NavVerticalGroup';
import NavVerticalItem from './NavVerticalItem';
import NavVerticalLink from './NavVerticalLink';
import { RouteConfigComponentProps } from 'react-router-config';
import { Location } from 'history';


const useStyles = makeStyles((theme: Theme): StyleRules => ({
  root: {
    padding : 0,
    '&.open': {
      backgroundColor: 'rgba(0,0,0,.08)'
    }
  },
  item: {
    height      : 40,
    width       : 'calc(100% - 16px)',
    borderRadius: '0 20px 20px 0',
    paddingRight: 12,
    color       : theme.palette.text.primary,
    '&.square'  : {
      width       : '100%',
      borderRadius: '0'
    }
  }
}));

interface Props extends RouteConfigComponentProps {
  item: Navigation;
  location: Location;
}

const needsToBeOpened = (location: Location, item: Navigation): boolean => location && isUrlInChildren(item, location.pathname);

const isUrlInChildren = (parent: Navigation, url: string): boolean => {
  if (!parent.children) {
    return false;
  }

  for (let i = 0; i < parent.children.length; i++) {
    if (parent.children[i].children) {
      if (isUrlInChildren(parent.children[i], url)) {
        return true;
      }
    }

    const myUrl = parent.children[i].url;

    if (myUrl === url ||(myUrl && url.includes(myUrl))) {
      return true;
    }
  }
  return false;
};

const _NavVerticalCollapse: React.FC<Props> = (props: Props) => {
  const classes = useStyles();
  const [open, setOpen] = useState(() => needsToBeOpened(location, props.item));
  const { item, location } = props;

  useEffect(() => {
    if (needsToBeOpened(location, item)) {
      setOpen(true);
    }
  }, [location, item]);

  const handleClick = (): void => setOpen(!open);

  return (
    <ul className={clsx(classes.root, open && 'open')}>
      <ListItem
        button
        className={clsx(classes.item, 'pl-24 list-item')}
        onClick={handleClick}>
        {item.icon &&
          <Icon color="action" className="text-16 flex-shrink-0 mr-16">{item.icon}</Icon>
        }
        <ListItemText className="list-item-text" primary={item.title} classes={{ primary: 'text-14' }} />
        {item.badge &&
          <NavigationBadge className="mr-4" badge={item.badge} />
        }
        <IconButton disableRipple className="w-16 h-16 p-0">
          <Icon className="text-16 arrow-icon" color="inherit">
            {open ? 'expand_less' : 'expand_more'}
          </Icon>
        </IconButton>
      </ListItem>

      {item.children &&
        <Collapse in={open} className="collapse-children">
          {
            item.children.map(innerItem =>

              <React.Fragment key={innerItem.id}>

                {innerItem.type === 'group' &&
                  <NavVerticalGroup item={innerItem} />
                }

                {innerItem.type === 'collapse' &&
                  <NavVerticalCollapse item={innerItem} />
                }

                {innerItem.type === 'item' &&
                  <NavVerticalItem item={innerItem} />
                }

                {innerItem.type === 'link' &&
                  <NavVerticalLink item={innerItem} />
                }

              </React.Fragment>
            )
          }
        </Collapse>
      }
    </ul>
  );
};

const NavVerticalCollapse = withRouter(React.memo(_NavVerticalCollapse));

export default NavVerticalCollapse;
