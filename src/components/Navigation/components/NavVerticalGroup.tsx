import React from 'react';
import { ListSubheader } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import clsx from 'clsx';
import { Navigation } from '../../../configs/navigationConfigs';
import NavVerticalCollapse from './NavVerticalCollapse';
import NavVerticalItem from './NavVerticalItem';
import NavVerticalLink from './NavVerticalLink';
import { RouteConfigComponentProps } from 'react-router-config';

const useStyles = makeStyles({
  item: {
    height      : 40,
    width       : 'calc(100% - 16px)',
    borderRadius: '0 20px 20px 0',
    paddingRight: 12
  }
});

interface Props extends RouteConfigComponentProps {
  item: Navigation;
}

const _NavVerticalGroup: React.FC<Props> = (props: Props) => {
  const classes = useStyles();
  const { item } = props;

  return (
    <>
      <ListSubheader disableSticky={true} className={clsx(classes.item, 'pl-24 list-subheader flex items-center')}>
        <span className="list-subheader-text uppercase text-12">
            {item.title}
        </span>
      </ListSubheader>

      {item.children &&
        <>
          {
            item.children.map(innerItem =>

              <React.Fragment key={innerItem.id}>

                {innerItem.type === 'group' &&
                  <NavVerticalGroup item={innerItem} />
                }

                {innerItem.type === 'collapse' &&
                  <NavVerticalCollapse item={innerItem} />
                }

                {innerItem.type === 'item' &&
                  <NavVerticalItem item={innerItem} />
                }

                {innerItem.type === 'link' &&
                  <NavVerticalLink item={innerItem} />
                }

              </React.Fragment>
            )
          }
        </>
      }
    </>
  );
};

const NavVerticalGroup = withRouter(React.memo(_NavVerticalGroup));

export default NavVerticalGroup;
