import React from 'react';
import { Divider, List } from '@material-ui/core';
import clsx from 'clsx';
import { Navigation } from '../../../configs/navigationConfigs';
import NavVerticalCollapse from './NavVerticalCollapse';
import NavVerticalGroup from './NavVerticalGroup';
import NavVerticalItem from './NavVerticalItem';
import NavVerticalLink from './NavVerticalLink';

interface Props {
  className: string;
  navigation: Navigation[]
}

const NavigationComponent: React.FC<Props> = (props: Props) => {
  const { navigation, className } = props;

  return (
    <List className={clsx('navigation whitespace-no-wrap', className)}>
      {
        navigation.map(item =>

          <React.Fragment key={item.id}>

            {item.type === 'group' &&
              <NavVerticalGroup item={item} />
            }

            {item.type === 'collapse' &&
              <NavVerticalCollapse item={item} />
            }

            {item.type === 'item' &&
              <NavVerticalItem item={item} />
            }

            {item.type === 'link' &&
              <NavVerticalLink item={item} />
            }

            {item.type === 'divider' &&
              <Divider className="my-16" />
            }
          </React.Fragment>
        )
      }
    </List>
  );
};

export default React.memo(NavigationComponent);
