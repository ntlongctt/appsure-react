import React from 'react';
import { Icon, ListItem, ListItemText, StyleRules, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import clsx from 'clsx';
import NavigationBadge from '../../NavigationBadge';
import { RouteConfigComponentProps } from 'react-router-config';
import { Navigation } from '../../../configs/navigationConfigs';

const useStyles = makeStyles((theme: Theme): StyleRules => ({
  item: {
    height                     : 40,
    width                      : 'calc(100% - 16px)',
    borderRadius               : '0 20px 20px 0',
    paddingRight               : 12,
    '&.active'                 : {
      backgroundColor            : theme.palette.secondary.main,
      color                      : theme.palette.secondary.contrastText + '!important',
      pointerEvents              : 'none',
      transition                 : 'border-radius .15s cubic-bezier(0.4,0.0,0.2,1)',
      '& .list-item-text-primary': {
        color: 'inherit'
      },
      '& .list-item-icon'        : {
        color: 'inherit'
      }
    },
    '&.square, &.active.square': {
      width       : '100%',
      borderRadius: '0'
    },
    '& .list-item-icon'        : {},
    '& .list-item-text'        : {},
    color                      : theme.palette.text.primary,
    textDecoration             : 'none!important'
  }
}));

interface Props extends RouteConfigComponentProps {
  item: Navigation;
}

const _NavVerticalLink: React.FC<Props> = (props: Props) => {
  const classes = useStyles(props);
  const { item } = props;

  return (
    <ListItem
      button
      component="a"
      href={item.url}
      target={item.target ? item.target : '_blank'}
      className={clsx(classes.item, 'pl-24 list-item')}>
      {item.icon &&
        <Icon className="list-item-icon text-16 flex-shrink-0 mr-16" color="action">{item.icon}</Icon>
      }
      <ListItemText className="list-item-text" primary={item.title} classes={{ primary: 'text-14 list-item-text-primary' }} />
      {item.badge &&
        <NavigationBadge badge={item.badge} />
      }
    </ListItem>
  );
};

const NavVerticalLink = withRouter(React.memo(_NavVerticalLink));

export default NavVerticalLink;
