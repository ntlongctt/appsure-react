import React from 'react';
import { NavLink } from 'react-router-dom';

interface Props {
  to: string;
  [key: string]: any;
}

const NavLinkAdapter = React.forwardRef((props: Props, ref: React.Ref<HTMLAnchorElement>) => {
  const { to, ...rest } = props;
  return (
    <NavLink innerRef={ref} to={to} {...rest} />
  )
});

export default NavLinkAdapter;
