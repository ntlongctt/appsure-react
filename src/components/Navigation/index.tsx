import React from 'react';
import clsx from 'clsx';
import NavigationComponent from './components/NavigationComponent';
import navigationConfigs from '../../configs/navigationConfigs';

interface Props {
  className?: string;
}

const Navigation = (props: Props): JSX.Element =>
    <NavigationComponent className={clsx('navigation', props.className)} navigation={navigationConfigs} />
  ;

export default Navigation;
