import React, { useState } from 'react';
import { Button, Icon, ListItemIcon, ListItemText, Popover, MenuItem, Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { ConnectState } from '../../models/ConnectState';
import { User } from '../../models/User';
import UserAvatar from '../UserAvatar';

const HeaderAvatar: React.FC<{}> = () => {
  const user: User = useSelector((state: ConnectState) => state.app.get('currentUser'));

  const [userMenu, setUserMenu] = useState<Element | null>(null);

  const userMenuClick = (event: React.MouseEvent): void => {
    setUserMenu(event.currentTarget);
  };

  const userMenuClose = (): void => {
    setUserMenu(null);
  };

  return (
    <>
      <Button className="h-64" onClick={userMenuClick}>
        <UserAvatar userName={user.name} src={user.avatar} />
        <div className="hidden md:flex flex-col ml-12 items-start">
          <Typography component="span" className="normal-case font-600 flex">
            {user.name}
          </Typography>
        </div>
        <Icon className="text-16 ml-12 hidden sm:flex">keyboard_arrow_down</Icon>
      </Button>

      <Popover
        open={Boolean(userMenu)}
        anchorEl={userMenu}
        onClose={userMenuClose}
        anchorOrigin={{
          vertical  : 'bottom',
          horizontal: 'center'
        }}
        transformOrigin={{
          vertical  : 'top',
          horizontal: 'center'
        }}
        classes={{
          paper: 'py-8'
        }}>
        <>
          <MenuItem onClick={userMenuClose}>
            <ListItemIcon className="min-w-40">
              <Icon>account_circle</Icon>
            </ListItemIcon>
            <ListItemText className="pl-0" primary="My Profile" />
          </MenuItem>
          <MenuItem onClick={userMenuClose}>
            <ListItemIcon className="min-w-40">
              <Icon>exit_to_app</Icon>
            </ListItemIcon>
            <ListItemText className="pl-0" primary="Logout" />
          </MenuItem>
        </>
      </Popover>
    </>
  );
};

export default HeaderAvatar;
