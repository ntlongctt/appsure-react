import React from 'react';
import { AppBar, Hidden, StyleRules, Theme, Toolbar } from '@material-ui/core';
import { makeStyles, ThemeProvider } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { ConnectState } from '../../models/ConnectState';
import HeaderAvatar from './HeaderAvatar';
import HeaderNotification from './HeaderNotification';
import NavbarMobileToggleButton from '../NavbarMobileToggleButton';
import { NavbarAction, navbarToggleMobile } from '../../store/navbar/action';

const useStyles = makeStyles((theme: Theme): StyleRules => ({
  separator: {
    width          : 1,
    height         : 64,
    backgroundColor: theme.palette.divider
  }
}));

const Header: React.FC<{}> = () => {
  const classes = useStyles();
  const headerTheme = useSelector((state: ConnectState) => state.setting.get('toolbarTheme'));
  const dispatch = useDispatch();

  const openMobileNavbar = (): NavbarAction => dispatch(navbarToggleMobile());

  return (
    <ThemeProvider theme={headerTheme}>
      <AppBar id="fuse-toolbar" className="relative z-10" color="default">
        <Toolbar className="p-0 flex justify-between">
          <div className="flex">
            <Hidden lgUp>
              <NavbarMobileToggleButton
                onClick={openMobileNavbar}
                className="w-64 h-64 p-0"
                />
              <div className={classes.separator} />
            </Hidden>
          </div>
          <div className="flex">
            <Hidden smDown>
              <HeaderNotification />
            </Hidden>
            <div className={classes.separator} />
            <HeaderAvatar />
          </div>
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  )
};

export default Header;
