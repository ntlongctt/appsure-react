import React from 'react';
import { Badge, Button, Icon } from '@material-ui/core';

const HeaderNotification: React.FC<{}> = () =>
    <>
      <Button className="h-64">
        <Badge badgeContent={5} color="error">
          <Icon className="text-28">
            notifications_active
          </Icon>
        </Badge>
      </Button>
    </>
  ;

export default HeaderNotification;
